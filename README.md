# Ultimate GitOps Instructor and Classroom Prep Checklists

The checklists are implemented as issue templates. You can either copy open an issue here with the template or copy the issue template to a place where you want to work on the checklist.

[1 Instructor Preparation](.gitlab/issue_templates/1 Instructor Preparation.md) is the suggested one-time preparation to become instructor ready.
> **DOUBLED EFFICIENCY:** It also makes you Audible and Demo ready (and results in a demo environment) for GitLab Agent for Kubernetes and Doing GitOps with GitLab Ultimate features.

[2 Per-Delivery Checklist](.gitlab/issue_templates/2 Per-Delivery Checklist.md) is the per delivery checklist so that you don't have to remember this all in your head each time.
> **DOUBLED EFFICIENCY:** Familiarizes you with the two-project template that the GitOps opinion of the workshop is based on - many built in patterns for production use - so also allows acceleration of customers during GitLab Agent / GitOps POVs.

[3 Classroom Management Tips](.gitlab/issue_templates/3 Classroom Management Tips.md) - classroom management guidance.

[4 Common Participant Questions and Answers](Typical Student Questions.md) - Q & A collected from actual classes.

