**Name The New Issue Like This (substitute changeset version):** `Instructor Preparation for YOUR NAME`

### Pre-Flight (10 Days Before)

#### Reading and Listening

**Also preps for**: To be demo and audible ready on “How GitLab does K8s Pull GitOps” - including minimal background on Pull GitOps itself.

- [ ] 15m Understand and Practice Google Slides 'Presenter Mode'[Overview](https://www.customguide.com/google-slides/google-slides-presenter-view), [Next Level Tools](https://www.brightcarbon.com/blog/how-to-present-in-google-slides-with-present-mode-toolbar/) - needed for slides that are designed to be presented using 'progressive disclosure' of complex visuals.
- [ ] 15m [Review Combined Classroom Slides / Delivery Guide](https://view-su2.highspot.com/viewer/62e3cb6ffe01256217b26539) - the student slides also guide the instructor on when to do all classroom management transitions like demos, breaks, exercises to complete, etc.
- [ ] 60m [Listen to complete video talk track of delivery of the above slides](https://view-su2.highspot.com/viewer/62e3cb6ffe01256217b26539)
- [ ] 15m Scan through [Lab Section 4](https://gitlab-for-eks.awsworkshop.io/040_gitlab_gitops_via_agent.html) and [Lab Section 5](https://gitlab-for-eks.awsworkshop.io/050_review_env_security_scanning.html).

#### Doing Classroom Prep

**Also preps for**: to be demo and POC support ready on GitLab Agent infrastructure. 

Note: For workshops co-delivered with AWS as part of their workshops program, the AWS account and cluster will be provisioned for you.

- [ ] 15m [Be Audible Ready on Why The Classroom Examples Require Public Groups and Projects, but that is not a requirement of the GitLab Agent itself](Requirement for Public Groups and Projects for Group Level GitLab Agent for Kubernetes Integration for This Working Example)
- [ ] 5m [+60m wait] [Perform Lab Section 2.1 to Prepare a Cluster in AWS](https://gitlab-for-eks.awsworkshop.io/020_gitlab_integrated_eks/021_deploy_eks_quickstart.html)
- [ ] 5m [+20m wait] [Perform Prep Lab 2.2 to Deploy a Scaling Runner](https://gitlab-for-eks.awsworkshop.io/020_gitlab_integrated_eks/022_prepare_gitlab_group_and_runner.html)
- [ ] 40m [+50m wait] [Perform Prep Lab 2.3 to Deploy Agent, Ingress and Cert Manager for Classgroup](https://gitlab-for-eks.awsworkshop.io/020_gitlab_integrated_eks/023_integrate_eks_with_gitlab_agent_for_ci_push.html)

#### Doing Participant Experience

**Also preps for**: To be demo and POC support ready on GitLab Agent project structure. 

- [ ] 60m [+20m wait] [Perform Lab Section 4.x](https://gitlab-for-eks.awsworkshop.io/040_gitlab_gitops_via_agent.html)
- [ ] 60m [+40m wait] [Perform Lab Section 5.x](https://gitlab-for-eks.awsworkshop.io/050_review_env_security_scanning.html)


#### Tailoring The Class To Yourself

- [ ] Decide which of the slides in the deck you are going to cover and how verbose your explanation of each will be - use your judgement as some slides are more critical to understanding the workshop content than others (for instance the new capabilities that the GitLab Agent gives to GitLab)

/assign [instructor gl id]
/subscribe @l_lopez @darwinjs [FMM gl id]