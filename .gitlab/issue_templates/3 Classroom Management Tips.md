

### Keeping the Class On Track (Instructor)
- [ ] On the first exercise break explain that a) the exercises are very detailed for a reason and b) have had a lot of debugging and that c) they should be followed in detail d) even by students who know the system well. Explain that you yourself tend to make errors when straying from the exercises.
- [ ] When breaking for exercises, always put the exercise that people should be working from and to and the prospective class restart time somewhere that everyone can see it all the time (Whiteboard for in-person, Chat for virtual) - otherwise you spend a lot of time answering that exact question.
- [ ] Ask folks to turn over their nametag (virtual: us a persistent indicator) when they are done exercises - then at a glance you can understand if students are on-pace for exercise completion or whether you should extend the lab period.
- [ ] When explaining what options to pick during an exercise, it can be helpful to demo the actual exercise page so that it is very clear.
- [ ] Encourage all participants to be diligent in remembering what step they are working on - sometimes a set of steps is repeated in two different contexts and so it can be easy to get confused.
- [ ] Participants who have challenges keeping their place in exercises will need more help.
- [ ] Advanced participants may also have difficulty because they feel inclined to use their existing knowledge rather than the detailed exercise steps - just keep pointing them back to the guide. If they find an error - thank them and be sure to [report it as an issue as indicated here](https://gitlab-for-eks.awsworkshop.io/010_introduction/reporting_problems_or_features.html).

### Assisting Participants (Instructor and Assistants)
- [ ] **Always, always, always** have the student identify the EXACT exercise step they are having trouble with and read it and the items before it before starting to help. Always.
