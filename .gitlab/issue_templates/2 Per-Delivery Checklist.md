
**Name The New Issue Like This (substitute changeset version):** `Classroom Preparation for: Location, Date and Time`

### Event Logitics
- Location:
- Date and Time with Timezone: 
- Marketing Issue Link  (do a formal link in "Linked items" after creating the issue as well):
- GitLab Instructor: _REPLACE_WITH_GL_INSTRUCTOR_MENTION_
- Classgroup (after creating in steps below):

### [3-4 Weeks Before] Event Logistics 

#### Co-ordinating with the AWS In-Classroom Support Team

* Can be done by marketing manager or instructors / assistants

- [ ] All GitLab Team Members who will be at the event should create an amazon.com (yes, the shopping website) user id with their gitlab email.
- [ ] Install the AWS Chime client
- [ ] In Chime, Add the relevant AWS and GitLab Team member emails to your Chime 'Contacts'
- [ ] Create a new Chime room that clearly indicates your event date and location - e.g. "2022-09-21 Ultimate GitOps Workshop in Philadelphia"
- [ ] Add all the AWS and GitLab Team Members to the room (it will only pick from the AWS Chime Contacts). Try to avoid inviting to Chime via email if possible.

### [1 Days Before] Pre-Flight

#### Doing Classroom Prep

**Also preps for**: to be demo and POC support ready on GitLab Agent infrastructure. 

Note: For workshops co-delivered with AWS as part of their workshops program, the AWS account and cluster will be provisioned for you.

IMPORTANT: AWS Event Engine accounts provided by AWS have a hard expirey of 72 hours. This means classroom technical prep can only happen 1 day before hand or at most, 2 days. This is an absolutely hard limit - be sure not to have AWS create the account at at time that causes it to expire during classroom delivery. Keep global timezones in mind.

- [ ] Create a classgroup in Learn Labs Structure (can be many days before)
    - [ ] @DarwinJS Create a classgroup under https://gitlab.com/gitlab-learn-labs/gitops and make note of the group name in the above Logistics details. Ensure @mkriaf and _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ are added as members
    - [ ] OR for Non-AWS Co-Delivery Create a gitlab.com group anywhere you have access to a full ultimate license.
    - [ ] OR for Non-AWS Co-Delivery Create a gitlab.com namespace and enable an Ultimate Trial WARNING: You will need to use Personal Access Tokens for one of the labs.
* [ ] @mkriaf 5m [+60m wait] [Perform or verify as complete - Lab Section 2.1 to Prepare a Cluster in AWS](https://gitlab-for-eks.awsworkshop.io/020_gitlab_integrated_eks/021_deploy_eks_quickstart.html)
* [ ] @mkriaf 5m Email account login credentials to @DarwinJS and _REPLACE_WITH_GL_INSTRUCTOR_MENTION_
* [ ] _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ 5m [+20m wait] [Perform or verify as complete - Prep Lab 2.2 to Deploy a Scaling Runner](https://gitlab-for-eks.awsworkshop.io/020_gitlab_integrated_eks/022_prepare_gitlab_group_and_runner.html) Runner tokens should be retrieved from the "Classgroup" noted in the **Event Logitistics** section above.
* [ ] _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ 40m [+50m wait] [Perform Prep Lab 2.3 to Deploy Agent, Ingress and Cert Manager for Your classgroup](https://gitlab-for-eks.awsworkshop.io/020_gitlab_integrated_eks/023_integrate_eks_with_gitlab_agent_for_ci_push.html)
* [ ] _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ Check if there have been any changes to the delivery slide deck since you last instructed

#### Create Working Example Which Also Verifies Everything is Working.

The classroom has some dynamic dependencies on things that are periodically upgraded - such as the Cluster Management project, AutoDevOps Components like SAST and Auto Deploy and the version of GitLab.com. It is therefore necessary to test exercise. This has the side benefits of:
- renewing your familiarity if it has been a while and if exercises have been updated to remove errors or simplify
- Participants can also use the project you crate as a working ready reference while completing labs.

Create a subgroup under classgroup called "Working Example" (slug: working-example)

- [ ] _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ 60m [+20m wait] [Perform Lab Section 4.x Up Through 4.5](https://gitlab-for-eks.awsworkshop.io/040_gitlab_gitops_via_agent.html) - 4.6 is unnecessary unless you want the working example background color to match the labs exactly.

### [1 Day Before] Pre-flight
- [ ] _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ Pack your laptop powersupply.
- [ ] _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ Pack needed connection dongles for external projection.
- [ ] _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ Confirm with @l_lopez and/or regional marketing manager that exercises are planned to be pre-printed at AWS site.

### [1-2 hrs Before Event, at delivery site] Pre-flight 

#### Instructor Computer by Instructor: _REPLACE_WITH_GL_INSTRUCTOR_MENTION_ 
- [ ] 15m Verify network connectivity of **instructor machine** and ability to connect to the following locations (validating you are not firewalled or network restricted at the delivery location)
  - [ ] Login to https://console.aws.amazon.com
  - [ ] Login to the bastion using SSM via [these instructions.](https://gitlab-for-eks.awsworkshop.io/010_introduction/tuning_and_troubleshooting.html#using-the-eks-bastion-for-cluster-administration-with-kubectl-and-helm)
  - [ ] Login to https://gitlab.com and access classgroup
  - [ ] Start a zoom meeting if the classroom is a blended live / web delivery or if receiving instructor support via zoom. This works best if the projector is an extended monitor, not mirrored with your desktop.

#### If there are GitLab Assistants, these steps can be done by them on any computer
- [ ] If receiving remote instructor support, create a GitLab slack channel for the delivery 
  - [ ] Add all GitLab classroom assistants and the instructor to the channel.
  - [ ] @mention whomever will be remote supporting you and ask for confirmation that they have received your message.
- [ ] 10m If you turned off any of the ASGs, [reenable them for the right level of scale](https://gitlab-for-eks.awsworkshop.io/090_appendices/051_cluster_ops.html) - 2 EKS Nodes, 1 Runner Per 5 students.
- [ ] If you have the list of gitlab.com ids for participants, add them to the 'classgroup' with the `Maintainer` role, otherwise this can be performed quickly before the first lab assignment by yourself or a classroom assistant.

/assign @mkriaf @DarwinJS _REPLACE_WITH_GL_INSTRUCTOR_MENTION_
